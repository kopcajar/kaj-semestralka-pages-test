import Player from 'http://127.0.0.1:5501/js/Entities/Player.js';
import { player, room, camera, context, canvas } from 'http://127.0.0.1:5501/js/main.js';
import Map from 'http://127.0.0.1:5501/js/Game/Map.js';
import NPC from 'http://127.0.0.1:5501/js/Entities/NPC.js';

export default class Game {
    static controls = {
        left: false,
        up: false,
        right: false,
        down: false
    };

    static FPS = 30;
    static INTERVAL = 1000 / Game.FPS;
    static STEP = Game.INTERVAL / 1000;

    static runningId = -1;
    static firstTimeStart = true;

    static togglePause() {
        if (Game.runningId === -1) {
            Game.play();
        } else {
            clearInterval(Game.runningId);
            Game.runningId = -1;
            console.log("Paused");
            this.saveGameState()
            $(".game-endmenu").show();
            $("#gameCanvas").hide();
        }
    }

    static play() {
        if (Game.runningId === -1) {
            Game.runningId = setInterval(() => {
              $(".game-endmenu").hide();
              $("#gameCanvas").show();
                Game.gameLoop();
            }, Game.INTERVAL);
            console.log("Game started");
            Game.firstTimeStart = false;
        }
    }

    static gameLoop() {
        Game.update();
        Game.draw();

        // Check if all NPCs in current stage are defeated
        if (room.map.areNPCsDefeated()) {
            room.map.progressToNextStage(); // Progress to the next stage
        }
    }

    static update() {
        player.update(Game.STEP, room.width, room.height, room.map);
        room.map.objects.filter(object => {
            if (object instanceof NPC){ object.update(Game.STEP, player, 50)}});
        camera.update();
    }

    static draw() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        room.map.draw(context, camera.xView, camera.yView);
        player.draw(context, camera.xView, camera.yView);
    }

    static saveGameState() {
        const gameState = {
            player: {
                coins: player.coins,
                upgrades: player.upgrades,
                maxHealth: player.maxHealth,
                speed: player.speed,
                attackSpeed: player.attackSpeed,
                damage: player.damage
                // Add other properties as needed
            }
            // Add other game state data if necessary
        };

        localStorage.setItem('gameState', JSON.stringify(gameState));
        console.log('Game state saved:', gameState);
    }

    static loadGameState() {
        const savedGameState = localStorage.getItem('gameState');
        if (savedGameState) {
            const gameState = JSON.parse(savedGameState);

            // Update player object with saved data
            player.x = 2000;
            player.x = 2000;
            player.coins = gameState.player.coins;
            player.upgrades = gameState.player.upgrades;
            player.maxHealth = gameState.player.maxHealth;
            player.health = player.maxHealth;
            player.speed = gameState.player.speed;
            player.attackSpeed = gameState.player.attackSpeed;
            player.damage = gameState.player.damage;
            // Update other game state data as needed

            console.log('Game state loaded:', gameState);
            Game.firstTimeStart = false; // Set firstTimeStart to false
            return true; // Indicate successful loading
        } else {
            console.log('No saved game state found.');
            return false; // Indicate no saved state found
        }
    }

    static startNewGame() {
        if(Game.loadGameState()){
          if (Game.firstTimeStart){
            console.log("Starting a new game for the first time.");
            Game.play
          }else{
            room.map = new Map(4000, 4000)
            room.map.generate()
            // Continue with loaded game state
            Game.play();
          }
        }else{
          console.log("Failed to load saved game state. Starting a new game.");
          // Handle scenario where saved game state failed to load
          Game.play(); // Start a new game if loading failed
        }
    }

    static endGame() {
        // Save game state when the game ends
        Game.saveGameState();
        clearInterval(Game.runningId);
        Game.runningId = -1;
        console.log("Game ended. Game state saved.");

        // Display end game UI, e.g., buttons to restart or go to menu
        $(".game-endmenu").show();
        $("#gameCanvas").hide();
    }
}
