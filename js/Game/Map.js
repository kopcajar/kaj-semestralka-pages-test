import GameObject from 'http://127.0.0.1:5501/js/Objects/GameObject.js';
import NPC from 'http://127.0.0.1:5501/js/Entities/NPC.js';
import DestructibleObject from 'http://127.0.0.1:5501/js/Objects/DestructibleObject.js';
import Chest from 'http://127.0.0.1:5501/js/Objects/Chest.js';
import Building from 'http://127.0.0.1:5501/js/Objects/Building.js';
import Tree from 'http://127.0.0.1:5501/js/Objects/Tree.js';
import Wall from 'http://127.0.0.1:5501/js/Objects/Wall.js';
import Item from 'http://127.0.0.1:5501/js/Objects/Item.js';
import Stage from 'http://127.0.0.1:5501/js/Game/Stage.js';

export default class Map {
    constructor(width, height) {
      this.width = width;
      this.height = height;
      this.image = new Image();
      this.objects = []; // Array to hold objects on the map
      this.spawnedNPCs = []; // Keep track of spawned NPCs
      this.stages = [
        new Stage(1, 5, 50,100, 10 ,50, "http://127.0.0.1:5501/textures/flamingo.svg"),   // Stage 1: 5 NPCs, health 50, damage 10
        new Stage(2, 8, 75,100, 15,50, "http://127.0.0.1:5501/textures/kuma.svg"),   // Stage 2: 8 NPCs, health 75, damage 15
        new Stage(3, 10, 100,100, 20,50, "http://127.0.0.1:5501/textures/chopper.svg"), // Stage 3: 10 NPCs, health 100, damage 20
        // Add more stages as needed
    ];
    this.currentStageIndex = 0;
    this.currentStage = this.stages[this.currentStageIndex];
    }

 
  
  
  
  
    generate() {
      this.image.onload = () => {
        // Once the image is loaded, you can draw it directly onto the canvas
        const canvas = document.createElement("canvas");
        canvas.width = this.width;
        canvas.height = this.height;
        const ctx = canvas.getContext("2d");
  
        // Draw the background image from top-left corner to cover the entire map
        ctx.drawImage(this.image, 0, 0, this.width, this.height);
  
        // Generate other map elements
        this.generateObjects();
  
        // Assign the canvas as the map's image
        this.image = canvas;
      };
  
      // Generate chests
      this.generateChests();
  
       // Generate NPCs
       this.generateNPCs();
  
       // Generate destructible objects
       this.generateDestructibleObjects();
  
      // Set the src after onload to ensure that the onload event is triggered
      this.image.src = "http://127.0.0.1:5501/textures/background7.svg";
    }
      
  
    // Generate objects such as buildings, trees, chests, enemies, etc.
  generateObjects() {
    // Example: Generate buildings
    let xx = 300
    let yy = 300
    this.objects.push(new Building(xx, yy-100, 100, 100)); // Example building at (100, 100) with width and height of 100
    this.objects.push(new Tree(xx, yy, 50, 50)); 
  
    this.objects.push(new Wall(xx-125, yy, 50, 200));
    this.objects.push(new Wall(xx-75, yy-75, 50, 50));
  
    this.objects.push(new Wall(xx+75, yy-75, 50, 50));
    this.objects.push(new Wall(xx+125, yy, 50, 200));
  
    // Example: Generate trees
    // this.objects.push(new Tree(200, 200, 50, 50)); // Example tree at (200, 200) with width and height of 50
    this.objects.push(new Tree(1400, 1400, 150, 150)); 
    // this.objects.push(new Tree(2000, 2000, 50, 50)); // Example tree at (200, 200) with width and height of 50
  
    // Add more object generation logic as needed
  }
  
  generateChests() {
    // Example: Generate chests
    let xx = 800;
    let yy = 800;
    const chest = new Chest(xx, yy, 50, 40);
    chest.addItem(new Item("Gold Coin"));
    chest.addItem(new Item("Health Potion"));
    this.objects.push(chest);
  }
  
  generateNPCs() {
    for (let i = 0; i < this.currentStage.npcCount; i++) {
        const xx = Math.random() * this.width;  // Random position within map bounds
        const yy = Math.random() * this.height; // Random position within map bounds
        const npc = new NPC(xx, yy, 50, 50, this.currentStage.npcHealth,this.currentStage.npcSpeed, this.currentStage.npcDamage, 50, this.currentStage.npcTexture, 100);
        this.objects.push(npc);
        this.spawnedNPCs.push(npc);
    }
}

  areNPCsDefeated() {
  return this.spawnedNPCs.length < (this.currentStage.npcCount * 0.2)
  }


  // Method to progress to the next stage
  progressToNextStage() {
    if (this.currentStageIndex < this.stages.length - 1) {
        this.currentStageIndex++;
        this.currentStage = this.stages[this.currentStageIndex];
        this.generateNPCs(); // Regenerate NPCs for the new stage
    } else {
        console.log("Final stage reached!"); // Handle final stage logic here
    }
}
  
  generateDestructibleObjects() {
    // Example: Generate destructible objects
    let xx = 1000;
    let yy = 1000;
    // this.objects.push(new DestructibleObject(xx, yy, 50, 50, 30)); // Example destructible object
  }


  addObject(object){
    this.objects.push(object)
  }

  removeObject(object) {
    const index = this.objects.indexOf(object);
    if (index > -1) {
        this.objects.splice(index, 1);
        if (object instanceof NPC){
          this.spawnedNPCs.splice(this.spawnedNPCs.indexOf(object),1)
        }
        console.log(`Object removed: ${object.constructor.name}`);
    } else {
        console.log("Object not found in map.");
    }
}
  
  draw(context, xView, yView) {
    context.drawImage(this.image, xView, yView, Math.min(this.width, context.canvas.width), Math.min(this.height, context.canvas.height), 0, 0, Math.min(this.width, context.canvas.width), Math.min(this.height, context.canvas.height));
  
    // Draw objects on the map
    this.objects.forEach(object => {
      object.draw(context, xView, yView);
      });
  }
  }