import GameObject from 'http://127.0.0.1:5501/js/Objects/GameObject.js';
import EXP from 'http://127.0.0.1:5501/js/Objects/EXP.js';
import { room } from 'http://127.0.0.1:5501/js/main.js';
import Weapon from 'http://127.0.0.1:5501/js/Objects/Weapon.js';

export default class NPC extends GameObject {
    constructor(x, y, width, height, health, speed, attackDamage, attackRange, npcTexture, attackCooldown) {
        super(x, y, width, height);
        this.health = health;
        this.speed = speed;
        this.attackDamage = attackDamage;
        this.attackRange = attackRange;
        this.npcTexture = npcTexture;
        this.svgImage = new Image();
        this.svgImage.src = npcTexture;
        this.weapon = new Weapon("npc", 10 ,"melee", 50, "http://127.0.0.1:5501/textures/fist.svg", 2000)
        this.facingDirection = "right";
    }

    receiveDamage(damage) {
        this.health -= damage;
        if (this.health <= 0) {
            this.health = 0;
            this.onDeath();
        }
    }

    onDeath() {
        room.map.removeObject(this);
        console.log("NPC died!");
        // Drop EXP upon death
        const exp = new EXP(this.x, this.y, 20, 20, 10); // Example EXP object
        room.map.addObject(exp);
    }

    moveTowardsPlayer(player, step) {
        const dx = player.x - this.x;
        const dy = player.y - this.y;
        const distance = Math.sqrt(dx * dx + dy * dy);

        if (distance < 10) return;

        const directionX = dx / distance;
        const directionY = dy / distance;

        this.x += directionX * this.speed * step;
        this.y += directionY * this.speed * step;
    }

    attackPlayer(player, deltaTime) {
        if (!this.weapon || !this.weapon.canAttack()) return;
 

        const dx = player.x - this.x;
        const dy = player.y - this.y;
        const distance = Math.sqrt(dx * dx + dy * dy);
        this.facingDirection = dx >= 0 ? "right" : "left";

        if (this.weapon.type === 'melee') {
            this.performMeleeAttack(dx, dy, distance, player);
        } else if (this.weapon.type === 'ranged') {
            this.performRangedAttack(dx, dy);
        }
    }



    performMeleeAttack(dx, dy, distance, player) {
        const attackRange = this.weapon.range;
        if (distance <= attackRange) {
            this.weapon.startAttack();
            player.receiveDamage(this.attackDamage);
            console.log(`NPC dealt ${this.attackDamage} damage to Player`);
            console.log(`Player HP ${player.health}r`)
            console.log(this)
        }
    }

    performRangedAttack(dx, dy) {
        console.log(`Shooting projectile towards (${dx}, ${dy})`);
    }


    update(step, player, deltaTime) {
        this.moveTowardsPlayer(player, step);
        this.attackPlayer(player, deltaTime);
        this.weapon.update();
    }

    draw(context, xView, yView) {
        const x = this.x - this.width / 2 - xView;
        const y = this.y - this.height / 2 - yView;
        context.drawImage(this.svgImage, x, y, this.width, this.height);

        // Draw health bar
        const healthBarWidth = this.width * (this.health / 100);
        context.fillStyle = "green";
        context.fillRect(x, y - 10, healthBarWidth, 5);
    }
}
