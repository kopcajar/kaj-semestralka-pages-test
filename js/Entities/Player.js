import GameObject from 'http://127.0.0.1:5501/js/Objects/GameObject.js';
import Weapon from 'http://127.0.0.1:5501/js/Objects/Weapon.js';
import Item from 'http://127.0.0.1:5501/js/Objects/Item.js';
import NPC from 'http://127.0.0.1:5501/js/Entities/NPC.js';
import DestructibleObject from 'http://127.0.0.1:5501/js/Objects/DestructibleObject.js';
import EXP from 'http://127.0.0.1:5501/js/Objects/EXP.js';
import Game from 'http://127.0.0.1:5501/js/Game/Game.js';
import { room } from 'http://127.0.0.1:5501/js/main.js';

export default class Player {
    constructor(x, y, map) {
        this.map = map;
        this.x = x;
        this.y = y;
        this.cursorX = x;
        this.width = 50;
        this.height = 50;
        this.health = 100;
        this.maxHealth = 100;
        this.speed = 200;
        this.followingCursor = false;
        this.svgImage = new Image();
        this.svgImage.src = "http://127.0.0.1:5501/textures/player.svg";
        this.weapon = new Weapon("Fist", 10, "melee", 100, "http://127.0.0.1:5501/textures/fist.svg", 500);
        this.facingDirection = "right";
        this.exp = 0;
        this.level = 1;
        this.coins = 10000;
        this.upgrades = {
            maxHealth: 1,
            speed: 1,
            attackSpeed: 1,
            damage: 1
        };
        this.upgradePrices = {
            maxHealth: [100, 200, 300, 400, 500],
            speed: [100, 200, 300, 400, 500],
            attackSpeed: [100, 200, 300, 400, 500],
            damage: [100, 200, 300, 400, 500]
        };
    }

    

    receiveDamage(damage) {
        this.health -= damage;
        if (this.health <= 0) {
            this.health = 0;
            this.onDeath();
        }
    }

    onDeath() {
        console.log("Player died!");
        Game.endGame()
    }


    upgrade(upgradeType){
        if (this.coins >= this.upgradePrices[upgradeType][this.upgrades[upgradeType] - 1] && this.upgrades[upgradeType] < 5) {
            this.coins = this.coins - this.upgradePrices[upgradeType][this.upgrades[upgradeType] - 1];
            this.upgrades[upgradeType]++
            this.updateValues()
        }
    }

    // Function to update player values based on upgrades
    updateValues() {
        // Update max health
        this.maxHealth = this.maxHealth + this.upgrades.maxHealth * 100;

        // Update speed
        this.speed = this.speed + this.upgrades.speed * 2;

        // Update attack speed 
        this.attackSpeed = this.attackSpeed - this.upgrades.attackSpeed * 100;

        // Update damage
        this.damage = this.damage + this.upgrades.damage * 5;

        console.log("Player values updated:", this);

        Game.saveGameState();
}


    attack(mouseX, mouseY) {
        if (!this.weapon || !this.weapon.canAttack()) return;

        const dx = mouseX - this.x;
        const dy = mouseY - this.y;
        const distance = Math.sqrt(dx * dx + dy * dy);
        this.facingDirection = dx >= 0 ? "right" : "left";
        this.weapon.startAttack();

        if (this.weapon.type === 'melee') {
            this.performMeleeAttack(dx, dy, distance);
        } else if (this.weapon.type === 'ranged') {
            this.performRangedAttack(dx, dy);
        }
    }

    performMeleeAttack(dx, dy, distance) {
        const attackRange = this.weapon.range;
        room.map.objects.forEach(object => {
            if ((object instanceof NPC || object instanceof DestructibleObject) &&
                this.isInAttackRange(object, attackRange)) {
                object.receiveDamage(this.weapon.damage);
                console.log(`Dealt ${this.weapon.damage} damage to ${object.constructor.name}`);
            }
        });
    }

    performRangedAttack(dx, dy) {
        console.log(`Shooting projectile towards (${dx}, ${dy})`);
    }

    isInAttackRange(target, attackRange) {
        const dx = target.x - this.x;
        const dy = target.y - this.y;
        const distance = Math.sqrt(dx * dx + dy * dy);
        return distance <= attackRange;
    }

    followCursor(x, y) {
        this.cursorX = x;
        this.cursorY = y;
        this.followingCursor = true;
    }

    stopFollowingCursor() {
        this.followingCursor = false;
    }

    update(step, worldWidth, worldHeight, map) {
        this.move(step,map)
        this.updateFacingDirection();
        this.weapon.update();
        this.constrainToWorldBounds(worldWidth, worldHeight);
    }



    levelUp() {
        const expThreshold = this.level * 100; // Example threshold
        if (this.exp >= expThreshold) {
            this.level += 1;
            this.exp -= expThreshold;
            console.log(`Leveled up to ${this.level}`);
        }
    }

    move(step, map) {
        let dx = 0;
        let dy = 0;
    
        if (Game.controls.left) {
            dx -= this.speed * step;
        }
        if (Game.controls.right) {
            dx += this.speed * step;
        }
        if (Game.controls.up) {
            dy -= this.speed * step;
        }
        if (Game.controls.down) {
            dy += this.speed * step;
        }
    
        // Normalize direction vector
        const distance = Math.sqrt(dx * dx + dy * dy);
        if (distance !== 0) {
            dx /= distance;
            dy /= distance;
        }
    
        let newX = this.x + dx * this.speed * step;
        let newY = this.y + dy * this.speed * step;
    
        const collidedObjects = this.getCollidedObjects(newX, newY, map);
    
        let collisionX = false;
        let collisionY = false;
    
        collidedObjects.forEach(object => {
            if (object instanceof NPC) {
                return;
            }
            if (object instanceof EXP) {
                // Collect EXP and level up
                this.exp += object.expValue;
                map.removeObject(object);
                console.log(`Collected ${object.expValue} EXP`);
                this.levelUp();
            }
            if (this.checkCollision(this.x, newY, map)) {
                collisionY = true;
            }
            if (this.checkCollision(newX, this.y, map)) {
                collisionX = true;
            }
        });
    
        if (collisionX && collisionY) {
            newX = this.x;
            newY = this.y;
        } else if (collisionX) {
            newX = this.x;
        } else if (collisionY) {
            newY = this.y;
        }
    
        this.x = newX;
        this.y = newY;
    }

    updateFacingDirection() {
        const dx = this.cursorX - this.x;
        this.facingDirection = dx >= 0 ? "right" : "left";
    }

    

    constrainToWorldBounds(worldWidth, worldHeight) {
        this.x = Math.max(this.width / 2, Math.min(this.x, worldWidth - this.width / 2));
        this.y = Math.max(this.height / 2, Math.min(this.y, worldHeight - this.height / 2));
    }

    checkCollision(x, y, map) {
        const collidedObjects = this.getCollidedObjects(x, y, map);
        return collidedObjects.length > 0;
    }

    getCollidedObjects(x, y, map) {
        const playerObject = new GameObject(x, y, this.width, this.height);
        return map.objects.filter(object => playerObject.collide(object));
    }

    getWeapon() {
        return this.weapon;
    }

    draw(context, xView, yView) {
        context.save();
    
        // Flip horizontally if facing left
        // if (this.facingDirection === "left") {
        //     context.scale(-1, 1); // Flip the context horizontally
        // }
    
        // Draw the player image
        if (this.facingDirection === "right") {
            context.drawImage(this.svgImage, this.x - this.width / 2 - xView, this.y  - this.height / 2 - yView, this.width, this.height);
            // context.drawImage(this.svgImage, weaponX + weaponOffsetX - playerWidth / 2 - xView, weaponY + weaponOffsetY - playerHeight / 2 - yView, playerWidth, playerHeight);
        } else {
            // Flip the image for left direction
            context.scale(-1, 1);
            context.drawImage(this.svgImage, -(this.x + this.width / 2 - xView), this.y  - this.height / 2 - yView, this.width, this.height);
            // context.drawImage(this.svgImage, -(weaponX - weaponOffsetX + playerWidth / 2 - xView), weaponY + weaponOffsetY - playerHeight / 2 - yView, playerWidth, playerHeight);
            context.scale(-1, 1);
        }


        // context.drawImage(this.svgImage, (this.facingDirection === "left" ? -1 : 1) * (this.x - this.width / 2 - xView), this.y - this.height / 2 - yView, this.width, this.height);
    
        context.restore();
    
        if (this.weapon.isAttacking) {
            // Draw the weapon
            this.weapon.draw(context, xView, yView, this.x, this.y, this.width, this.height, this.facingDirection);
        }
    
        // Draw health bar
    const healthPercentage = this.health / this.maxHealth;
    const healthBarWidth = this.width * healthPercentage;

    // Calculate health bar color
    const red = Math.floor((1 - healthPercentage) * 255);
    const green = Math.floor(healthPercentage * 255);
    const healthBarColor = `rgb(${red},${green},0)`;

    context.fillStyle = healthBarColor;
    context.fillRect(this.x - this.width / 2 - xView, this.y - this.height / 2 - yView - 10, healthBarWidth, 5);
    }
    
    
}
