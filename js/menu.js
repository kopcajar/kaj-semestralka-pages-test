import main from './main.js';
import { player } from './main.js';
import Game from './Game/Game.js'; 

$(document).ready(function() {
    $("#game-start").show();
    $("#game-mainmenu").hide();
    $("#gameCanvas").hide();
    $(".game-upgrademenu").hide(); 
    $(".game-endmenu").hide(); 

    // Start button click handler
    $("#startButton").click(function() {
        $("#game-start").hide();
        $("#game-mainmenu").show();
    });

    // Start Game button click handler
    $("#startGameButton").click(function() {
        $("#game-mainmenu").hide();
        $("#gameCanvas").show();
        startGame();
    });

    // Upgrade Menu button click handler
    $("#upgradeMenuButton").click(function() {
        $("#game-mainmenu").hide();
        $(".game-upgrademenu").show(); 
        $("#playerCoins").text(`Coins: ${player.coins}`);
        generateUpgradeItems();
    });

    // Settings button click handler
    $("#settingsButton").click(function() {
        console.log("Settings menu is not implemented yet.");
    });

    // Back to Main Menu button click handler
    $("#backToMainMenuButton").click(function() {
        Game.saveGameState();
        $(".game-upgrademenu").hide(); 
        $("#game-mainmenu").show();
    });

    // Restart Game button click handler
    $("#restartButton").click(function() {
        $(".game-endmenu").hide();
        Game.startNewGame();
    });

    // Pause Game button click handler
    $("#pauseButton").click(function() {
        Game.togglePause();
    });

    // Resume Game button click handler
    $("#resumeButton").click(function() {
        $(".game-endmenu").hide();
        Game.startNewGame();
    });

    $("#backToMenuButton").click(function() {
        Game.endGame();
        $(".game-endmenu").hide();
        $("#game-mainmenu").show();
    });

    function startGame() {
        main(); // Assuming this initializes the game
        console.log("Game started");
    }

    function generateUpgradeItems() {
        // Assuming `player` is accessible here
        const upgrades = Object.keys(player.upgrades);
    
        // Clear any existing items
        $("#upgradeItemsContainer").empty();
    
        upgrades.forEach(upgradeType => {
            const level = player.upgrades[upgradeType];
            const cost = player.upgradePrices[upgradeType][level - 1];
            const maxLevel = 5;
            const upgradeItem = `
                <div class="upgrade-item" data-upgrade="${upgradeType}">
                    <span>${upgradeType.charAt(0).toUpperCase() + upgradeType.slice(1)} (Level ${level})</span>
                    <span>Cost: ${cost} coins</span>
                    <button class="upgrade-button" ${level >= maxLevel ? 'disabled' : ''}>Upgrade</button>
                </div>
            `;
            $("#upgradeItemsContainer").append(upgradeItem);
        });
    
        // Attach event handlers to dynamically created buttons
        $(".upgrade-button").click(function() {
            const upgradeType = $(this).parent().data("upgrade");
            upgradePlayer(upgradeType);
        });
    }
    
    function upgradePlayer(upgradeType) {
        player.upgrade(upgradeType);
        $("#playerCoins").text(`Coins: ${player.coins}`);
        generateUpgradeItems(); // Refresh the upgrade items to reflect changes
    }
    

    function upgradePlayer(upgradeType) {
        if (player.coins >= 50 && player.upgrades[upgradeType] < 5) {
            player.upgrade(upgradeType);
            $("#playerCoins").text(`Coins: ${player.coins}`);
            generateUpgradeItems(); // Refresh the upgrade items to reflect changes
        }
    }

    // Drag and drop functionality
    $(".weapon-item").on("dragstart", function(e) {
        e.originalEvent.dataTransfer.setData("weaponType", $(this).data("weapon"));
    });

    $(".equipment-slot").on("dragover", function(e) {
        e.preventDefault();
        $(this).addClass("drag-over");
    });

    $(".equipment-slot").on("dragleave", function(e) {
        $(this).removeClass("drag-over");
    });

    $(".equipment-slot").on("drop", function(e) {
        e.preventDefault();
        $(this).removeClass("drag-over");
        const weaponType = e.originalEvent.dataTransfer.getData("weaponType");
        player.equipWeapon(weaponType);
        console.log(`Equipped weapon: ${weaponType}`);
    });
});
