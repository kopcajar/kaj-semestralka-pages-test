import DestructibleObject from 'http://127.0.0.1:5501/js/Objects/DestructibleObject.js';

export default class Chest extends DestructibleObject {
    constructor(x, y, width, height) {
        super(x, y, width, height);
        this.items = []; // Array to hold items in the chest
        this.svgImage = new Image();
        this.svgImage.src = "http://127.0.0.1:5501/textures/chest.svg";
    }
  
    addItem(item) {
        this.items.push(item);
    }
  
    open() {
        // Simulate opening the chest (e.g., play animation, etc.)
        // For now, just log the items contained
        console.log("Opening chest:");
        this.items.forEach(item => {
            console.log(item.name);
        });
    }
  
    draw(context, xView, yView) {
        // Draw the chest
        const x = this.x - this.width / 2 - xView;
        const y = this.y - this.height / 2 - yView;
        context.drawImage(this.svgImage,x, y, this.width, this.height);
    }
  }