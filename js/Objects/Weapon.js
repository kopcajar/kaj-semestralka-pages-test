export default class Weapon {
    constructor(name, damage, type, range, src, baseAttackDuration) {
        this.name = name;
        this.damage = damage;
        this.type = type;
        this.range = range;
        this.svgImage = new Image();
        this.svgImage.src = src;
        this.baseAttackDuration = baseAttackDuration; // Base duration of the attack animation in milliseconds
        this.attackDuration = baseAttackDuration; // Actual attack duration considering player's attack speed
        this.isAttacking = false;
        this.attackTimer = 0;
    }

    startAttack() {
        this.isAttacking = true;
        this.attackTimer = Date.now();
    }

    update() {
        if (this.isAttacking && Date.now() - this.attackTimer > this.attackDuration) {
            this.isAttacking = false;
        }
    }

    canAttack() {
        return !this.isAttacking;
    }

    updateAttackDuration(attackSpeedMultiplier) {
        this.attackDuration = this.baseAttackDuration * attackSpeedMultiplier;
    }

    draw(context, xView, yView, playerX, playerY, playerWidth, playerHeight, facingDirection) {
        if (!this.isAttacking) return;

        context.save();

        let weaponX = playerX;
        let weaponY = playerY;
        const weaponOffsetX = 30; // Offset for the weapon position (adjust as needed)
        const weaponOffsetY = -20; // Offset for the weapon position (adjust as needed)

        if (facingDirection === "right") {
            context.drawImage(this.svgImage, weaponX + weaponOffsetX - playerWidth / 2 - xView, weaponY + weaponOffsetY - playerHeight / 2 - yView, playerWidth, playerHeight);
        } else {
            // Flip the image for left direction
            context.scale(-1, 1);
            context.drawImage(this.svgImage, -(weaponX - weaponOffsetX + playerWidth / 2 - xView), weaponY + weaponOffsetY - playerHeight / 2 - yView, playerWidth, playerHeight);
            context.scale(-1, 1);
        }

        context.restore();
    }
}
