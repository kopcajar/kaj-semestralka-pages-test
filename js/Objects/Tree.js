import GameObject from 'http://127.0.0.1:5501/js/Objects/GameObject.js';

export default class Tree extends GameObject {
    constructor(x, y, width, height) {
        super(x, y, width, height);
    }
  
    draw(context, xView, yView) {
        // Adjust coordinates so that the central point of the tree corresponds to its x and y coordinates
        const x = this.x - this.width / 2 - xView;
        const y = this.y - this.height / 2 - yView;
  
        context.fillStyle = "#008000"; // Dark green color for trees
        context.fillRect(x, y, this.width, this.height);
    }
  }