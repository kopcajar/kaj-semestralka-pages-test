import GameObject from 'http://127.0.0.1:5501/js/Objects/GameObject.js';

export default class Building extends GameObject {
    constructor(x, y, width, height) {
        super(x, y, width, height);
    }
  
    draw(context, xView, yView) {
        // Adjust coordinates so that the central point of the building corresponds to its x and y coordinates
        const x = this.x - this.width / 2 - xView;
        const y = this.y - this.height / 2 - yView;
  
        context.fillStyle = "#A9A9A9"; // Dark gray color for buildings
        context.fillRect(x, y, this.width, this.height);
    }
  }